#include <stdio.h>
#include <stm32f10x.h>


#define CLOCK 8000000



void init(USART_TypeDef * ,double );
unsigned char  read_char (USART_TypeDef * );
void write (USART_TypeDef *,unsigned char );
void read_string (USART_TypeDef * ,char *);
void write_string (USART_TypeDef * ,char* );


