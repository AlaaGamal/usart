#include "USART.h"
#include "GPIO.h"



void init(USART_TypeDef * USARTX,double BOUD_RATE){
	

		if(USARTX == USART1){
			
			RCC->APB2ENR|=(1 << 14);    //enabel usart clock
			RCC->APB2ENR|=(1 << 2);     //enable portA clock
			GPIO_INIT_HIGH(GPIOA,0x000004b0);//make pin 9 as output and pin 10 as input
			
		}else if(USARTX == USART2){
			
				RCC->APB1ENR|=(1 << 17);    //enabel usart clock
				RCC->APB2ENR|=(1 << 2);     //enable portA clock
				GPIO_INIT_LOW(GPIOA,0x00000b40);//make pin 2 as output and pin 1 as input

		}else if(USARTX == USART3){
				
				RCC->APB1ENR|=(1 << 18);    //enabel usart clock
				RCC->APB2ENR|=(1 << 3);     //enable portB clock
				GPIO_INIT_HIGH(GPIOB,0x00004b00);//make pin 10 as output and pin 11 as input

		}	
		USARTX->CR1 |=(1<<13)|(1<<3)|(1<<2);
		USARTX->CR1 &=~(1<<12) ;
		USARTX->CR2 &=~(1<<12)& ~(1<<13);
	  USARTX->BRR=CLOCK/BOUD_RATE;  
}
/////////////////////////////////////////////////////////////////////////
unsigned char  read_char(USART_TypeDef * USARTX){
	  while (!(USARTX->SR & (1<<5)));
		return (USARTX->DR & 0x000001ff) ;
	
}
////////////////////////////////////////////////////////////////////////
void write (USART_TypeDef * USARTX,unsigned char data){
	while (!(USARTX->SR & (1<<7)));
	USART1->DR=data;
}
////////////////////////////////////////////////////////////////////////
void write_string (USART_TypeDef * USARTX,char *data){
	int i=0;
	while(data[i] != '\0'){
		write(USARTX,data[i]);
		i++;
	}
	write(USARTX,data[i]);
}
///////////////////////////////////////////////////////////////////////
void read_string (USART_TypeDef * USARTX,char *data){
	int i=0;
	data[i]=read_char(USARTX);
	while(data[i]!= '\0'){
		data[i]=read_char(USARTX);
		i++;
	}
	data[i]='\0';
}
